﻿namespace CO.Common.ViewModels
{
    public class PersonalListVM
    {
        public string CompanyShortName { get; set; }
        public string CompanyName { get; set; }
        public int Price { get; set; }

        public PersonalListVM()
        {
        }

        public PersonalListVM(string companyShortName, string companyName, int price)
        {
            CompanyShortName = companyShortName;
            CompanyName = companyName;
            Price = price;
        }
    }
}