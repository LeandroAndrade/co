﻿using System.Collections.Generic;

namespace CO.Common.ViewModels
{
    public class HomeQuotesVM
    {
        public List<QuoteVM> GeneralList { get; set; }
        public List<PersonalListVM> PersonalList { get; set; }
    }
}