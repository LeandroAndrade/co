﻿namespace CO.Common.ViewModels
{
    public class QuoteVM
    {
        public int CompanyId { get; set; }
        public string CompanyShortName { get; set; }
        public string CompanyName { get; set; }
        public int Price { get; set; }

        public QuoteVM()
        {
        }

        public QuoteVM(int companyId, string companyShortName, string companyName, int price)
        {
            CompanyId = companyId;
            CompanyShortName = companyShortName;
            CompanyName = companyName;
            Price = price;
        }
    }
}