﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Context;

namespace CO.Infra.DataPersistence.Config
{
    public class SessionHelper : ISessionHelper
    {
        private readonly string _connectionString;
        private ISessionFactory _sessionFactory;

        public SessionHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ISessionFactory SessionFactory
            => _sessionFactory ?? (_sessionFactory = CreateSessionFactory(_connectionString));

        public ISession CreateSession()
        {
            var sessionFactory = SessionFactory ?? CreateSessionFactory(_connectionString);

            if (!CurrentSessionContext.HasBind(sessionFactory))
            {
                var session = sessionFactory.OpenSession();
                CurrentSessionContext.Bind(session);
            }

            return sessionFactory.GetCurrentSession();
        }

        private static ISessionFactory CreateSessionFactory(string connectionString)
        {
            return
                Fluently.Configure()
                    .Database(
                        MsSqlConfiguration.MsSql2012.ConnectionString(connectionString))
                    .CurrentSessionContext("web")
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ISessionHelper>())
                    .BuildSessionFactory();
        }
    }
}