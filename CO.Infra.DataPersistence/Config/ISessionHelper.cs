﻿using NHibernate;

namespace CO.Infra.DataPersistence.Config
{
    public interface ISessionHelper
    {
        ISessionFactory SessionFactory { get; }
        ISession CreateSession();
    }
}