using CO.Core;
using CO.Core.DomainService;
using CO.Infra.DataPersistence.Base;
using NHibernate;

namespace CO.Infra.DataPersistence
{
    public class UserStockListRepository : BaseRepository<UserStockList>, IUserStockListRepository
    {
        public UserStockListRepository(ISession session) : base(session)
        {
        }
    }
}