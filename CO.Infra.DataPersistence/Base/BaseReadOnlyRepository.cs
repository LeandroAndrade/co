﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CO.Core.DomainService;
using NHibernate;
using NHibernate.Linq;

namespace CO.Infra.DataPersistence.Base
{
    public abstract class BaseReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class
    {
        protected readonly ISession Session;

        protected BaseReadOnlyRepository(ISession session)
        {
            Session = session;
        }

        public virtual TEntity GetById(int entityId)
        {
            return Session.Get<TEntity>(entityId);
        }

        public virtual List<TEntity> GetAll(int limit = 1000)
        {
            return All(limit).ToList();
        }

        public virtual TEntity GetBy(Expression<Func<TEntity, bool>> expression)
        {
            var returnQuery = FilterBy(expression).FirstOrDefault();
            return returnQuery;
        }

        public virtual List<TEntity> GetManyBy(Expression<Func<TEntity, bool>> expression, int limit = 1000)
        {
            return FilterBy(expression, limit).ToList();
        }

        private IQueryable<TEntity> FilterBy(Expression<Func<TEntity, bool>> expression, int limit = 1000)
        {
            var allQuery = All(limit).Where(expression);
            return allQuery;
        }

        private IQueryable<TEntity> All(int limit)
        {
            return Session.Query<TEntity>().Take(limit);
        }
    }
}