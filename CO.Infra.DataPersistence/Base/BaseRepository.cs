﻿using CO.Core.DomainService;
using NHibernate;

namespace CO.Infra.DataPersistence.Base
{
    public abstract class BaseRepository<TEntity> : BaseReadOnlyRepository<TEntity>, IRepository<TEntity>
        where TEntity : class
    {
        protected BaseRepository(ISession session) : base(session)
        {
        }

        public virtual void CreateOrUpdate(TEntity model)
        {
            Session.SaveOrUpdate(model);
        }

        public virtual TEntity CreateOrUpdateReturnEntity(TEntity model)
        {
            Session.SaveOrUpdate(model);
            Session.Flush();
            return model;
        }

        public virtual void Delete(int id)
        {
            var objectToRemove = Session.Get<TEntity>(id);
            Session.Delete(objectToRemove);
        }

        public virtual void RemoveFromCache(TEntity model)
        {
            Session.Evict(model);
        }
    }
}