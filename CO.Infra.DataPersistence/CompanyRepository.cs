﻿using CO.Core;
using CO.Core.DomainService;
using CO.Infra.DataPersistence.Base;
using NHibernate;

namespace CO.Infra.DataPersistence
{
    public class CompanyRepository : BaseReadOnlyRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(ISession session) : base(session)
        {
        }
    }
}