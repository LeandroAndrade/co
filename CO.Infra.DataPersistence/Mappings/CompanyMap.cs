﻿using CO.Core;
using FluentNHibernate.Mapping;

namespace CO.Infra.DataPersistence.Mappings
{
    public class CompanyMap : ClassMap<Company>
    {
        public CompanyMap()
        {
            Table("Companies");
            Id(x => x.Id);
            Map(x => x.ShortName);
            Map(x => x.Name);
        }
    }
}