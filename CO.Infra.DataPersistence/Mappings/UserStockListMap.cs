using CO.Core;
using FluentNHibernate.Mapping;

namespace CO.Infra.DataPersistence.Mappings
{
    public class UserStockListMap : ClassMap<UserStockList>
    {
        public UserStockListMap()
        {
            Table("UserStockList");
            Id(x => x.Id);
            Map(x => x.UserId);
            Map(x => x.CompanyId);
            Map(x => x.Sequence);
        }
    }
}