namespace CO.Core.DomainService
{
    public interface IUserStockListRepository : IRepository<UserStockList>
    {
    }
}