using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CO.Core.DomainService
{
    public interface IReadOnlyRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int entityId);
        List<TEntity> GetAll(int limit = 1000);
        TEntity GetBy(Expression<Func<TEntity, bool>> expression);
        List<TEntity> GetManyBy(Expression<Func<TEntity, bool>> expression, int limit = 1000);
    }
}