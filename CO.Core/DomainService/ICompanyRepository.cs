﻿namespace CO.Core.DomainService
{
    public interface ICompanyRepository : IReadOnlyRepository<Company>
    {
    }
}
