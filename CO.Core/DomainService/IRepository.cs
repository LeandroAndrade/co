namespace CO.Core.DomainService
{
    public interface IRepository<TEntity> : IReadOnlyRepository<TEntity> where TEntity : class
    {
        void CreateOrUpdate(TEntity model);
        TEntity CreateOrUpdateReturnEntity(TEntity model);
        void Delete(int id);
        void RemoveFromCache(TEntity model);
    }
}