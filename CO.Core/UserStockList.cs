namespace CO.Core
{
    public class UserStockList
    {
        public virtual int Id { get; set; }
        public virtual string UserId { get; set; }
        public virtual int CompanyId { get; set; }
        public virtual int Sequence { get; set; }
    }
}