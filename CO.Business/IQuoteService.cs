using CO.Common.ViewModels;

namespace CO.Business
{
    public interface IQuoteService
    {
        HomeQuotesVM GetQuotes();
    }
}