using System.Collections.Generic;
using System.Linq;
using CO.Common.ViewModels;
using CO.Core.DomainService;

namespace CO.Business.ConcreteServices
{
    public class QuoteService : IQuoteService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IUserStockListRepository _userStockListRepository;
        private readonly IPriceService _priceService;

        public QuoteService(ICompanyRepository companyRepository, IUserStockListRepository userStockListRepository,
            IPriceService priceService)
        {
            _companyRepository = companyRepository;
            _userStockListRepository = userStockListRepository;
            _priceService = priceService;
        }

        public HomeQuotesVM GetQuotes()
        {
            var companies = _companyRepository.GetAll();
            var userList = _userStockListRepository.GetManyBy(x => x.UserId == "1as4|");
            var homeQuotesVM = new HomeQuotesVM
            {
                GeneralList = new List<QuoteVM>(),
                PersonalList = new List<PersonalListVM>()
            };

            companies.ForEach(x =>
            {
                var price = _priceService.GetRandomPrice();
                homeQuotesVM.GeneralList.Add(new QuoteVM(x.Id, x.ShortName, x.Name, price));
            });

            userList.ForEach(x =>
            {
                var priceFromGeneralList =
                    homeQuotesVM.GeneralList.Single(c => c.CompanyId == x.CompanyId).Price;
                var company = companies.Single(c => c.Id == x.CompanyId);
                homeQuotesVM.PersonalList.Add(new PersonalListVM(company.ShortName, company.Name, priceFromGeneralList));
            });

            return homeQuotesVM;
        }
    }
}