using System;

namespace CO.Business.ConcreteServices
{
    public class PriceService : IPriceService
    {
        private readonly Random _randomPrice;
        private const int MinPrice = 1;
        private const int MaxPrice = 1000;

        public PriceService()
        {
            _randomPrice = new Random();
        }

        public int GetRandomPrice()
        {
            return _randomPrice.Next(MinPrice, MaxPrice);
        }
    }
}