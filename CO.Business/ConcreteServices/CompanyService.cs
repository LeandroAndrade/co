﻿namespace CO.Business.ConcreteServices
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyService _companyService;

        public CompanyService(ICompanyService companyService)
        {
            _companyService = companyService;
        }
    }
}