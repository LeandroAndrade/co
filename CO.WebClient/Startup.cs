﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CO.WebClient.Startup))]
namespace CO.WebClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
