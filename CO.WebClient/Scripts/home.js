﻿var CrossOver = CrossOver || {};

CrossOver.Home = function () {
    var self = this;
    var tblGeneralList = $("#generalList");
    var tblPersonalList = $("#personalList");
    var currentQuote = [];
    var lastQuote = [];

    self.loadGeneralList = function () {
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "/Home/GetGeneralQuoteList",
            success: function (data) {
                lastQuote = currentQuote;
                currentQuote = data.GeneralList;
                var personalList = data.PersonalList;
                var generalListBody = tblGeneralList.find("tbody");
                var personalListBody = tblPersonalList.find("tbody");
                generalListBody.html("");
                personalListBody.html("");

                $(currentQuote).each(function(idx, quote) {
                    var lastPrice = lastQuote.length === 0 ? "" : lastQuote[idx].Price.toFixed(2);
                    generalListBody.append(
                        "<tr>"
                        + "<td style='cursor: pointer;'><span data-toggle='tooltip' title='" + quote.CompanyName + "'>" + quote.CompanyShortName + "</span></td>"
                        + "<td>" + quote.Price.toFixed(2) + "</td>"
                        + "<td>" + lastPrice + "</td>"
                        + "</tr>"
                    );
                });

                $(personalList).each(function(idx, quote) {
                    personalListBody.append(
                        "<tr>"
                        + "<td>" + quote.CompanyShortName + "</td>"
                        + "<td>" + quote.Price.toFixed(2) + "</td>"
                        + "<td>" + 2 + "</td>"
                        + "</tr>"
                    );
                });
            }
        });
    };

    self.loadTooltip = function() {
        $("body").tooltip({ selector: "[data-toggle=tooltip]" });
    }
};

(function () {
    var home = new CrossOver.Home();
    home.loadGeneralList();
    home.loadTooltip();

    setInterval(function() {
        home.loadGeneralList();
    }, 10000);

})();