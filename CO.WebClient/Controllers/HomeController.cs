﻿using System.Security.Claims;
using System.Web.Mvc;
using CO.Business;

namespace CO.WebClient.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly IQuoteService _quoteService;

        public HomeController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }

        public ActionResult Index()
        {
            UserId = ClaimsPrincipal.Current.FindFirst("user_id").Value;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public JsonResult GetGeneralQuoteList()
        {
            //var randomPrice = new Random();

            //var homeQuotesVM = new HomeQuotesVM
            //{
            //    GeneralList = new List<QuoteVM>
            //    {
            //        new QuoteVM("MEAL3", randomPrice.Next(1, 1000)),
            //        new QuoteVM("MEAL4", randomPrice.Next(1, 1000)),
            //        new QuoteVM("MEAL5", randomPrice.Next(1, 1000)),
            //        new QuoteVM("MEAL6", randomPrice.Next(1, 1000)),
            //        new QuoteVM("MEAL7", randomPrice.Next(1, 1000)),
            //        new QuoteVM("MEAL8", randomPrice.Next(1, 1000))
            //    },
            //    PersonalList = new List<PersonalListVM>
            //    {
            //        new PersonalListVM("MEAL3", "International Food Corporation", 1),
            //        new PersonalListVM("MEAL4", "International Food Corporation", 2)
            //    },
            //};
            var quotes = _quoteService.GetQuotes();

            return Json(quotes, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPersonalList()
        {
            return Json("x");
        }
    }
}