﻿using System.Web.Mvc;

namespace CO.WebClient.Controllers
{
    public class BaseController : Controller
    {
        protected string UserId { get; set; }
    }
}