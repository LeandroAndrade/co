using System;
using System.Configuration;
using System.Web;
using CO.Business;
using CO.Business.ConcreteServices;
using CO.Core.DomainService;
using CO.Infra.DataPersistence;
using CO.Infra.DataPersistence.Config;
using CO.WebClient;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using NHibernate;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace CO.WebClient
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(new DependencyModule());
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            CreateNHibernateSessionFactory(kernel);
        }

        private static void CreateNHibernateSessionFactory(IKernel kernel)
        {
            var connString = ConfigurationManager.ConnectionStrings["CrossOverConnString"].ConnectionString;
            kernel.Bind<ISessionHelper>().To<SessionHelper>().WithConstructorArgument("connectionString", connString);
            var sessionHelper = kernel.Get<ISessionHelper>();
            var sessionFactory = sessionHelper.SessionFactory;

            kernel.Bind<ISessionFactory>().ToConstant(sessionFactory).InSingletonScope();
            kernel.Bind<ISession>().ToMethod(x => sessionHelper.CreateSession()).InRequestScope();

            //IoC.Configure(c => c.Resolve<ISessionFactory>().With(sessionFactory).AsSingleton());
            //IoC.Configure(c => c.Resolve<ISession>().ToMethod(sessionHelper.CreateSession).InRequestScope());
        }
    }

    public class DependencyModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserStockListRepository>().To<UserStockListRepository>();
            Bind<ICompanyRepository>().To<CompanyRepository>();

            Bind<ICompanyService>().To<CompanyService>();
            Bind<IPriceService>().To<PriceService>();
            Bind<IQuoteService>().To<QuoteService>();
        }
    }
}